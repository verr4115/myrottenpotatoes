Rails.application.routes.draw do
  resources :users
  get 'reviews/new'

  get 'reviews/create'

resources :movies #resources are kind of like data, which in this case are movies.    
    root :to => redirect('/movies')


resources :movies do
    resources :reviews
end 


get 'auth/twitter/callback' =>'sessions#create',:as => 'login'
get 'logout' => 'sessions#destroy'
get 'auth/failure' => 'sessions#failure'
    

get 'signup' => 'users#new'
post 'create' => 'users#create'

  
get 'login' => 'sessions#new', :as => 'loginuser'
post 'find' => 'sessions#find'

# For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
