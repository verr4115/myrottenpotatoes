class SessionsController < ApplicationController
  
  # user shouldn't have to be logged in before logging in!
  skip_before_action :set_current_user 
  
  def create
    # raise request.env["omniauth.auth"].to_yaml 
    
    auth=request.env["omniauth.auth"]
    user=Movieuser.find_by_provider_and_uid(auth["provider"],auth["uid"]) ||
      Movieuser.create_with_omniauth(auth)
    
    session[:user_id] = user.id
    session[:provider] = auth["provider"]
    session[:name] = auth[:info][:name]
    
    redirect_to movies_path
  end
  
  def destroy
    session[:user_id] = nil 
    session[:provider] = nil  # this is important, so if you log out and then login this is nil  
    @current_user = nil 
    
    flash[:notice] = 'Logged out successfully.'
    redirect_to movies_path
  end
  
  
  def new
    #user wants to log in 
  end 
  
  def find    
      
    #raise params.to_yaml
    
    #raise params[:user][:email].inspect

    user = User.find_by_email(params[:user][:email]) 

    if user && user.authenticate(params[:user][:password])
      session[:user_id] = user.id
      session[:provider] = nil   
      flash[:notice] = "logged in!" 
      redirect_to movies_path 
    else
      flash[:notice] = "Could not log in!" 
      redirect_to movies_path 
    end    

  end  


  
end